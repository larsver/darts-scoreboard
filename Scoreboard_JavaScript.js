let canvas = document.getElementById('canvas');
let WIDTH = Math.min(window.innerWidth, window.innerHeight);
let HEIGHT = WIDTH;
//canvas.width = window.innerWidth * 0.7;
//canvas.height = window.innerWidth * 0.7;
canvas.width = window.innerWidth * 0.5;
canvas.height = window.innerWidth * 0.5;
let RADIUS = (WIDTH - (WIDTH * 0.12))/2
let ID = {9: 6, 1: 8, 7: 4, 6: 18, 5: 1, 4: 20, 3: 5, 2: 12, 1: 9, 0: 14, 19: 11, 18: 8, 17: 16, 16: 7, 15: 19, 14: 3, 13: 17, 12: 2, 11: 15, 10: 10, 8: 13}
let DI = {10: 0, 15: 1, 2: 2, 17: 3, 3: 4, 19: 5, 7: 6, 16: 7, 8: 8, 11: 9, 14: 10, 9: 11, 12: 12, 5: 13, 20: 14, 1: 15, 18: 16, 4: 17, 13: 18, 6: 19}
let highlight = {1: [], 2: [], 3: [], 25: false, 50: false}
let grey = {1: [], 2: [], 3: [], 25: false, 50: false}

//let solPane = document.getElementById('sol');
let solTable


// Draw DartBoard
function drawBoard() {
	ctx = canvas.getContext('2d'); 
	ctx.clearRect(0, 0, WIDTH, HEIGHT);
	let arcLength = Math.PI/10 
	let offset = Math.PI/200
	red = false

	ctx.font = '20px Arial';
	for (let i = 0; i < 20; i++) {
		// Outer ring
		ctx.beginPath();
		if (highlight[2].includes(i)) {
		ctx.fillStyle = 'orange';
		} else if (grey[1].includes(i)) {
		ctx.fillStyle = 'grey';
		} else if (red) {
		ctx.fillStyle = 'green';
		} else {
		ctx.fillStyle = 'red';
		}
		ctx.lineTo(WIDTH/2, HEIGHT/2);
		ctx.arc(WIDTH/2, HEIGHT/2, RADIUS, arcLength/2 + arcLength * i + offset, arcLength/2 + arcLength * (i + 1) - offset);
		ctx.lineTo(WIDTH/2, HEIGHT/2);
		ctx.fill();

		// Inside outer ring
		ctx.beginPath();
		if (highlight[1].includes(i)) {
		ctx.fillStyle = 'orange';
		console.log(i);
		} else if (grey[1].includes(i)) {
		console.log(i);
		ctx.fillStyle = '#ebebeb';
		} else if (red) {
		ctx.fillStyle = 'white';
		} else {
		ctx.fillStyle = 'black';
		}
		ctx.lineTo(WIDTH/2, HEIGHT/2);
		ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.95, arcLength/2 + arcLength * i + offset, arcLength/2 + arcLength * (i + 1) - offset);
		ctx.lineTo(WIDTH/2, HEIGHT/2);
		ctx.fill();

		// Outer inside ring
		ctx.beginPath();
		if (highlight[3].includes(i)) {
		ctx.fillStyle = 'orange';
		} else if (grey[1].includes(i)) {
		ctx.fillStyle = 'grey';
		} else if (red) {
		ctx.fillStyle = 'green';
		} else {
		ctx.fillStyle = 'red';
		}
		ctx.lineTo(WIDTH/2, HEIGHT/2);
		ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.6, arcLength/2 + arcLength * i + offset, arcLength/2 + arcLength * (i + 1) - offset);
		ctx.lineTo(WIDTH/2, HEIGHT/2);
		ctx.fill();

		// Inside inner ring
		ctx.beginPath();
		if (highlight[1].includes(i)) {
		ctx.fillStyle = 'orange';
		} else if (grey[1].includes(i)) {
		ctx.fillStyle = '#ebebeb';
		} else if (red) {
		ctx.fillStyle = 'white';
		} else {
		ctx.fillStyle = 'black';
		}
		red = !red;

		ctx.lineTo(WIDTH/2, HEIGHT/2);
		ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.55, arcLength/2 + arcLength * i + offset, arcLength/2 + arcLength * (i + 1) - offset);
		ctx.lineTo(WIDTH/2, HEIGHT/2);
		ctx.fill();

		ctx.fillStyle = 'black';
		ctx.textAlign = 'center';
		let begin = arcLength * i + offset
		let textX = 1.06 * RADIUS * Math.cos(begin) + WIDTH/2;
		let textY = 1.06 * RADIUS * Math.sin(begin) + HEIGHT/2;
		let idx = ID[(i + 9)%20]
		// let idx = i;

		ctx.fillText(idx, textX, textY);
		// console.log(i, idx);
	}
	// Outer bullseye
	ctx.beginPath();
	if (highlight[25]) {
	ctx.fillStyle = 'orange';
	} else {
	ctx.fillStyle = 'green';
	}
	ctx.lineTo(WIDTH/2, HEIGHT/2);
	ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.10, 0, 2*Math.PI);
	ctx.lineTo(WIDTH/2, HEIGHT/2);
	ctx.fill();

	// Inner bullseye
	ctx.beginPath();
	if (highlight[50]) {
	ctx.fillStyle = 'orange';
	} else {
	ctx.fillStyle = 'red';
	}
	ctx.lineTo(WIDTH/2, HEIGHT/2);
	ctx.arc(WIDTH/2, HEIGHT/2, RADIUS*0.05, 0, 2*Math.PI);
	ctx.lineTo(WIDTH/2, HEIGHT/2);
	ctx.fill();
}

function coordToRadial(x, y) {
	x -= WIDTH/2
	y -= HEIGHT/2
	//x -= 0.0142 * WIDTH;  // We need to do this offset, for some reason.
	//y -= 0.0142 * HEIGHT;
	let rad = Math.atan2(y, x);
	let r = (x*x+y*y)**(1/2)

	let arcLength = Math.PI/10 
	let offset = Math.PI/200
	let id = Math.floor((rad-offset-arcLength/2)/(arcLength)) + 10;

	let mult = 0;
	let val = 0;
	let text = '';
	if (RADIUS * 0.95 <= r && r <= RADIUS * 1.00) {
	mult = 2;
	val = ID[id];
	text = 'double ' + val;
	} else if (RADIUS * 0.55 <= r && r <= RADIUS * 0.65) {
	mult = 3;
	val = ID[id];
	text = 'triple ' + val;
	} else if (RADIUS * 0.05 <= r && r <= RADIUS * 0.13) {
	mult = 1;
	val = 25;
	text = val;
	} else if (r <= RADIUS * 0.05) {
	mult = 2;
	val = 25,
	text = 'bullseye!';
	} else if (r <= RADIUS) {
	mult = 1;
	val = ID[id];
	text = 'single ' + val;
	} else {
	mult = 1;
	val = 0;
	text = 'miss';
	}
	return {'mult': mult, 'val': val,
		  'id': id, 'text': text};
}

var throws_in_turn = 0 ;
function clickHandler(e) {
	if ((player_turn == 1 || player_turn == 2) && throws_in_turn !=3) {
		
		const rect = canvas.getBoundingClientRect()
		plane = coordToRadial(e.clientX-rect.left, e.clientY-rect.top);
		console.log("plane:",plane);
		
		var total_input ;
		var input_field ;
		// get value of correct player inputfield
		if (player_turn == 1) {
			input_field = document.getElementById("invulscore1") ;
		}
		else {
			input_field = document.getElementById("invulscore2") ;
		}
		
		// calculate score
		total_input = input_field.value ;
		if (total_input == null || total_input == '') { 
			total_input = 0; 
		}
		total_input = parseInt(total_input) + parseInt(plane.val)*parseInt(plane.mult) ;
		
		// input can not be more than score and difference may not be one
		if ((player_turn == 1) && (parseInt(total_input) > parseInt(document.getElementById("score1").textContent) || parseInt(document.getElementById("score1").textContent)-parseInt(total_input)==1)){
				return 0 ;
		}
		if ((player_turn == 2) && (parseInt(total_input) > parseInt(document.getElementById("score2").textContent) || parseInt(document.getElementById("score2").textContent)-parseInt(total_input)==1)) {
				return 0 ;
		}
		
		// count throws in turn
		throws_in_turn += 1 ;
		// set value in inputfield
		input_field.value = total_input ;
	
		// draw circle on place
		ctx.beginPath();
		ctx.arc(e.clientX-rect.left, e.clientY-rect.top, 10, 0, 2 * Math.PI);
		ctx.lineWidth = 3;
		ctx.strokeStyle = '#FF00FF';
		ctx.stroke();
	}
}

// Get player names
var player1_name = sessionStorage.getItem("name1");
var player2_name = sessionStorage.getItem("name2");
document.getElementById("player1").textContent = player1_name ;
document.getElementById("player2").textContent = player2_name ;

// Global variables
var player_turn ;	// Keeps track of whose turn it is ('1' -> player1's turn, '2' -> player2's turn)
var InputScore1 ;	// Score inputfield of player1
var InputScore2 ;	// Score inputfield of player2
var arrow1 ;			// Errow to show it is player1's turn
var arrow2 ;			// Errow to show it is player2's turn
var aant_pijl ;		
var legs_speler1 ;	// Amount of legs won by player1
var legs_speler2 ;	// Amount of legs won by player2
var score1 ;		// Current remaining score for player1
var score2 ;		// Current remaining score for player2
var StartPlayer ;	// Keeps track of who may start the leg

var pl1_tot_pijlen ;	// Total amount of arrows throw by player1
var pl1_tot_score ;		// Total score made by player1
var pl2_tot_pijlen ;	// Total amount of arrows throw by player2
var pl2_tot_score  ;	// Total score made by player2

var gamestats ;		// Show or hide Gamestats html object
var keyboard ;		// Show or hide Keyboard html object


// score invullen disabelen
function InvulScoreCheck() {
	if (player_turn == 1) {
		InputScore1.disabled = false ;
		arrow1.style.display = "" ;
		InputScore2.disabled = true ;
		arrow2.style.display = "none" ;
		if (score1.textContent <= 170) {
			aant_pijl.style.display = "" ;
		}
		else {
			aant_pijl.style.display = "none" ;
		}
	}
	else if (player_turn == 2) {
		InputScore1.disabled = true ;
		arrow1.style.display = "none" ;
		InputScore2.disabled = false ;
		arrow2.style.display = "" ;
		if (score2.textContent <= 170) {
			aant_pijl.style.display = "" ;
		}
		else {
			aant_pijl.style.display = "none" ;
		}
	}
	else {
		InputScore1.disabled = true ;
		arrow1.style.display = "none" ;
		InputScore2.disabled = true ;
		arrow2.style.display = "none" ;
		aant_pijl.style.display = "none" ;
	}
}

// Onload body
function init() { 
	canvas.addEventListener('click', clickHandler);
	drawBoard();
	gamestats = document.getElementById("gamestats") ;
	keyboard = document.getElementById('keyboard') ;
	gamestats.style.display = "none" ;
	canvas.style.display = "none" ;

	InputScore1 = document.getElementById("invulscore1") ;
	InputScore2 = document.getElementById("invulscore2") ;
	arrow1 = document.getElementById("arrow1") ;
	arrow2 = document.getElementById("arrow2") ;
	aant_pijl = document.getElementById("aant_pijl") ;
	legs_speler1 = document.getElementById("legs_speler1") ;
	legs_speler2 = document.getElementById("legs_speler2") ;
	score1 = document.getElementById("score1") ;
	score2 = document.getElementById("score2") ;
	InvulScoreCheck() ;  

	StartPlayer = 1
	pl1_tot_pijlen = 0  ;
	pl1_tot_score = 0 ;
	pl2_tot_pijlen = 0;
	pl2_tot_score = 0 ;
}

// Change method of score input (keyboard or DartBoard) (onclick 'Input'-Button)
function changeInput(){
	if (canvas.style.display == "none" && keyboard.style.display !="none")
	{
		canvas.style.display = "block" ;
		keyboard.style.display ="none" ;
		gamestats.style.display = "none" ;
		InputScore1.value = "" ; 
		InputScore2.value = "" ;
		drawBoard() ;
		throws_in_turn = 0 ;
	}
	else 
	{
		canvas.style.display = "none" ;
		keyboard.style.display = "block"
		InputScore1.value = "" ; 
		InputScore2.value = "" ;
	}
}

// Showing or hidding the game stats (onclick 'Stats'-Button)
function openStats() {
	if (gamestats.style.display == "none" && canvas.style.display == "none"){
		gamestats.style.display = "flex" ;
	}
	else {
		gamestats.style.display = "none" ;
	}
}
// Start the game (onclick 'Start Game'-Button)
function StartGame() {
	player_turn = StartPlayer ;
	InvulScoreCheck() ;
	document.getElementsByName("start")[0].disabled = true ;
}

/* // End the game (onclick 'End Game"-Button)
function EndGame() {
	player_turn = 0 ;
	InvulScoreCheck() ;
	document.getElementsByName("end")[0].disabled = true ;
}*/

// Input of scores using Keyboard
function myKey(getal) {
	var total_input1 = document.getElementById("invulscore1").value ;
	var total_input2 = document.getElementById("invulscore2").value ;
	var nieuw_total_input1 = total_input1 + getal ;
	var nieuw_total_input2 = total_input2 + getal ;
	
	// ingegeven score verwijderen per 1
	if (getal == "remove") {
		InputScore1.value = InputScore1.value.substr(0, InputScore1.value.length - 1);
		InputScore2.value = InputScore2.value.substr(0, InputScore2.value.length - 1);
	}
	// ingegeven score ok toepassen
	else if (getal == "ok" ) {
		var score ;	
		if (player_turn == 1 && total_input1.length >0) { // speler1 beurt en er is iets ingegeven
			score = score1.textContent - total_input1; 	// score = de getoonde score - de ingegevenscore
			if (score >= 0 ) {						// als niet onder gaat
				score1.textContent = score ; 			// nieuwe score getoond
				modelExpand()
				player_turn = 2 ;					// andere speler aan de beurt
				Stats_throwlog(total_input1,score,1) ;	// stats aanpassen
				var arrow1 =  Aant_pijl_check(score) ;		// aantal aangegeven pijlen ophalen
				Gemiddeldes(1,total_input1,arrow1)	// gemiddelde berekenenen
			}
			InputScore1.value = "" ;					// ingegeven waarde verwijderen
		}
		else if (player_turn == 2 && total_input2.length >0) {	//speler2 beurt en er is iets ingegeven 
			score = score2.textContent - total_input2; 	// score = de getoonde score - de ingegevenscore
			if (score >= 0 ) { 						// als niet onder gaat
				score2.textContent = score ;			// nieuwe score getoond
				modelExpand()
				player_turn = 1 ;					// andere speler aan de beurt
				Stats_throwlog(total_input2,score,2) ;	// stats aanpassen
				var arrow2 =  Aant_pijl_check(score) ;		// aantal aangegeven pijlen ophalen
				Gemiddeldes(2,total_input2,arrow2)	// gemiddelde berekenenen
			}
			InputScore2.value = "" ;					// ingegeven waarde verwijderen
		}
		
		//voor board input
		drawBoard() ;
		throws_in_turn = 0 ;
		
	}
	// invullen score
	else {	
		// als player1
		if (player_turn == 1) {
			if (nieuw_total_input1 > 180) {
				InputScore1.value = total_input1 ;
			}
			else {
				InputScore1.value = nieuw_total_input1 ;
			}
		}
		// als player2
		else if (player_turn==2) {
			if (nieuw_total_input2 > 180) {
				InputScore2.value = total_input2 ;
			}
			else {
				InputScore2.value = nieuw_total_input2 ;
			}
		}	
	}	
	// radio button clearen
	var pijlen = document.getElementsByName("aant_pijl");
   	for(var i=0; i<pijlen.length;i++)
      	pijlen[i].checked = false;
	
	InvulScoreCheck() ;
	Reset() ;
}

// Check if someone won the leg
function Reset() {
	var stats_log = document.getElementsByClassName("test_log")
	if (score1.textContent == 0) {
		legs_speler1.textContent = parseInt(legs_speler1.textContent) + 1 ;
		score1.textContent = 501 ;
		score2.textContent = 501 ;
		while (stats_log.length > 0) {
			stats_log[0].remove();
		}
		Start_newLeg() ;
	}
	else if (score2.textContent == 0) {
		legs_speler2.textContent = parseInt(legs_speler2.textContent) + 1 ;
		score1.textContent = 501 ;
		score2.textContent = 501 ;
		while (stats_log.length > 0) {
			stats_log[0].remove();
		}
		Start_newLeg() ;
	}
}

// Switch StartPLayer for every new leg
function Start_newLeg() {
	if (StartPlayer == 1) {
		StartPlayer = 2 ;
		player_turn = 2 ;
	}
	else {
		StartPlayer = 1 ;
		player_turn = 1 ;
	}
}

// het aantal gegooide pijlen in beurt doorgeven
function Aant_pijl_check(hulp) { 
	var een = document.getElementById("1pijl").checked; 
	var twee = document.getElementById("2pijl").checked; 
	
	if (hulp == 0 ) {  // alleen wanneer exact op nul eindigt is het mogelijk om minder pijlen gebruikt te hebben
		if (een == true) {
			return 1 ;
		}
		else if (twee == true) {
			return 2 ;
		}
		else {
			return 3 ;
		}
	}
	else {
		return 3 ;
	}
}

// berekent en toont de gemmiddelde gegooide scores per pijl of per 3pijlen
function Gemiddeldes(speler,score,pijlen) {
	if (speler == 1 ) {
		pl1_tot_pijlen += pijlen ;
		pl1_tot_score += parseInt(score) ;
		var help1 = (pl1_tot_score / pl1_tot_pijlen) ;
		document.getElementById("pl1_gem3").textContent = (help1 * 3).toFixed(2) ;
		document.getElementById("pl1_gem1").textContent = help1.toFixed(2) ;
		document.getElementById("pl1_aant_pijl").textContent = pl1_tot_pijlen ;
	}
	else if (speler == 2) {
		pl2_tot_pijlen += pijlen ;
		pl2_tot_score += parseInt(score) ;
		var help2 = (pl2_tot_score / pl2_tot_pijlen) ;
		document.getElementById("pl2_gem3").textContent = (help2 * 3).toFixed(2) ;
		document.getElementById("pl2_gem1").textContent = help2.toFixed(2) ;
		document.getElementById("pl2_aant_pijl").textContent = pl2_tot_pijlen ;
	}
}

// tonen van gegooide waardes in stats
function Stats_throwlog(worp,score,speler) {
	var x = document.createElement('span');
	var y = document.createElement('span');
	x.setAttribute("class", "total-throw test_log");
	y.setAttribute("class", "total-score test_log");
	x.innerText = worp;
	y.innerText = score;
	
	if (speler == 1) {
		document.getElementById("speler1_throws").appendChild(x) ;
		document.getElementById("speler1_throws").appendChild(y) ;
		if (worp == 180) { //  als 180
			x.style.color = "green"
			x.style.fontWeight = "bold";
			document.getElementById("pl1_180").textContent = parseInt(document.getElementById("pl1_180").textContent) + 1 ;
		}
		else if (worp >= 160) { // als groter of gelijk dan 160
			document.getElementById("pl1_160").textContent = parseInt(document.getElementById("pl1_160").textContent) + 1 ;
		}
		else if (worp >= 140) { // als groter of gelijk dan 140
			document.getElementById("pl1_140").textContent = parseInt(document.getElementById("pl1_140").textContent) + 1 ;
		}
		else if (worp >= 120) { // als groter of gelijk dan 120
			document.getElementById("pl1_120").textContent = parseInt(document.getElementById("pl1_120").textContent) + 1 ;
		}
		else if (worp >= 100) { // als groter of gelijk dan 100
			document.getElementById("pl1_100").textContent = parseInt(document.getElementById("pl1_100").textContent) + 1 ;
		}
		else if (worp >= 80) { // als groter of gelijk dan 80
			document.getElementById("pl1_80").textContent = parseInt(document.getElementById("pl1_80").textContent) + 1 ;
		}
		else if (worp >= 60) { // als groter of gelijk dan 60
			document.getElementById("pl1_60").textContent = parseInt(document.getElementById("pl1_60").textContent) + 1 ;
		}
		else if (worp >= 0) { // als groter of gelijk dan 0
			document.getElementById("pl1_0").textContent = parseInt(document.getElementById("pl1_0").textContent) + 1 ;
		}
		if (parseInt(document.getElementById("pl1_high").textContent) < worp ) {
			document.getElementById("pl1_high").textContent = parseInt(worp) ;
		}	
		if (score == 0  && parseInt(document.getElementById("pl1_high_checkout").textContent)<worp) {
			document.getElementById("pl1_high_checkout").textContent = worp ;
		}
	}
	if (speler == 2) {
		document.getElementById("speler2_throws").appendChild(x) ;
		document.getElementById("speler2_throws").appendChild(y) ;
		if (worp == 180) { // rode kleur als 180
			x.style.color = "red"
			x.style.fontWeight = "bold";
			document.getElementById("pl2_180").textContent = parseInt(document.getElementById("pl2_180").textContent) + 1 ;
		}
		else if (worp >= 160) { // als groter of gelijk dan 160
			document.getElementById("pl2_160").textContent = parseInt(document.getElementById("pl2_160").textContent) + 1 ;
		}
		else if (worp >= 140) { // als groter of gelijk dan 140
			document.getElementById("pl2_140").textContent = parseInt(document.getElementById("pl2_140").textContent) + 1 ;
		}
		else if (worp >= 120) { // als groter of gelijk dan 120
			document.getElementById("pl2_120").textContent = parseInt(document.getElementById("pl2_120").textContent) + 1 ;
		}
		else if (worp >= 100) { // als groter of gelijk dan 100
			document.getElementById("pl2_100").textContent = parseInt(document.getElementById("pl2_100").textContent) + 1 ;
		}
		else if (worp >= 80) { // als groter of gelijk dan 80
			document.getElementById("pl2_80").textContent = parseInt(document.getElementById("pl2_80").textContent) + 1 ;
		}
		else if (worp >= 60) { // als groter of gelijk dan 60
			document.getElementById("pl2_60").textContent = parseInt(document.getElementById("pl2_60").textContent) + 1 ;
		}
		else if (worp >= 0) { // als groter of gelijk dan 0
			document.getElementById("pl2_0").textContent = parseInt(document.getElementById("pl2_0").textContent) + 1 ;
		}
		if (parseInt(document.getElementById("pl2_high").textContent) < worp ) {
			document.getElementById("pl2_high").textContent = parseInt(worp) ;
		}
	}
}


/////////////

/* Show Solutions */
function showSols(sols) {
	console.log("solutons",sols);
  for (let i = 1; i < 4; i++) {
    for (let solId = 0; solId < 3; solId++) {
      if (!sols.hasOwnProperty(solId)) {
        // In case less than 3 sols were found.
        solTable.rows[solId].cells[i-1].innerText = '/';
        continue
      }
      let val = sols[solId]['Throw' + i];
      let mult = sols[solId]['Mult' + i];
	  if (val == 0) {
		mult = '';
		val = '/';
	  }
      else if (mult == 3) {
        mult = 'T ';
      } else if (mult == 2) {
        mult = 'D ';
      } else if (mult == 1) {
        mult = 'S ';
      } else {
        mult = '';
      }
      solTable.rows[solId].cells[i-1].innerText = mult + val;
    }
  }
}

/* If no solution possible, display this. */
function noSols() {
  for (let i = 1; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      solTable.rows[i].cells[j].innerText = '/';
    }
  }
}

// IDP related stuff.
function initKb() {
  theory = `vocabulary V {
      //type nb := {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 50}
	  type nb := {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25}
      type mult := {1, 2, 3}
      Throw1: () -> nb
      Mult1: () -> mult
      Throw2: () -> nb
      Mult2: () -> mult
      Throw3: () -> nb
      Mult3: () -> mult
  
      Begin: () -> Int
      Score: () -> Int
      End: () -> Int
  
      Finish: () -> Bool
  }
  
  theory T:V {
  
      End() = Begin() - Score().
  
      [End score needs to be zero to finish]
      (Finish() <=> End() = 0).
  
      Score() = Throw1() * Mult1() + Throw2() * Mult2() + Throw3() * Mult3().
  
      Finish() & Throw3() ~= 0 => Mult3() = 2.
      Finish() & Throw3() = 0 & Throw2() ~= 0 => Mult2() = 2.
      Finish() & Throw3() = 0 & Throw2() = 0 & Throw1() ~= 0 => Mult1() = 2.

      //Throw1() = 25 | Throw1() = 50 => Mult1() = 1.
      //Throw2() = 25 | Throw2() = 50 => Mult2() = 1.
      //Throw3() = 25 | Throw3() = 50 => Mult3() = 1.
	  
	  // Single Bull and Double Bull (bullseye)
      Throw1() = 25 => Mult1() ~= 3.
      Throw2() = 25 => Mult2() ~= 3.
      Throw3() = 25 => Mult3() ~= 3.
	  
	  Throw2() = 0 => Mult2() = 1.
      Throw3() = 0 => Mult3() = 1.
	  
	  //Mult3() = 2 & Mult1() = Mult2() => Throw1() >= Throw2() .
	  //Mult1() = Mult2() => Throw1() >= Throw2().
      //Mult2() = Mult3() => Throw2() >= Throw3().
  }
  
  structure S:V {
  }
  
  procedure main() {
      pretty_print(model_expand(T,S,0))
  }
  `
  socket.send(JSON.stringify({'method': 'create',
                              'theory': theory}));
}

function initSocket() {
	console.log("initSocket");
  // socket = new WebSocket("ws://localhost:8765");
  if (navigator.userAgent.indexOf("Chrome") != -1) {
    // Use WSS for Chrome.
    socket = new WebSocket("wss://idpsocket.herokuapp.com/");
  } else {
    // Use WS else FF complains about HerokuApp's cert not being valid.
    // Should be fixed once we have proper hosting.
    socket = new WebSocket("ws://idpsocket.herokuapp.com/");
  }
  console.log("Socket =",socket);

  socket.onopen = function(e) {
    initKb();
  }
  socket.onmessage = function(event) {
    data = JSON.parse(event.data);
    if (data['method'] === 'modelexpand') {
      if (data['success']) {
			  console.log(data);
        //solPane.innerText = 'Finish possible!';
        showSols(data['models']);
      } else {
        //solPane.innerText = 'Cannot finish';
        noSols();
			  console.log(data);
      }
    } else if (data['method'] === 'propagate') {
      greyOut(data['negdict'])
      console.log(data);
    } else if (data['method'] === 'explain') {
    } else {
      console.log(data);
    }
  }
}

function setVal(symbol, val) {
	console.log("setVal");
  msg = {'method': 'setval',
         'symbol': symbol,
         'val': val};
  socket.send(JSON.stringify(msg));
}

function prepareIDP() {
	msg = {'method': 'setval',
		 'symbol': 'Finish',
		 'val': true};
	socket.send(JSON.stringify(msg));

	var val ;
	if (player_turn == 1) { 
		val = document.getElementById("score1").textContent; 
		solTable = document.getElementById('solTable1');
	}
	else { 
		val = document.getElementById("score2").textContent; 
		solTable = document.getElementById('solTable2');
	}
	msg = {'method': 'setval',
		 'symbol': 'Begin',
		 'val': val};
	socket.send(JSON.stringify(msg));
	
	// Update the already made throws.
	for (let i = 0; i < 3; i++) {
	  symb = 'Throw' + (i + 1);
	  msg = {'method': 'setval',
			 'symbol': symb,
			 'val': null};
	  symb = 'Mult' + (i + 1);
	  socket.send(JSON.stringify(msg));
	  msg = {'method': 'setval',
			 'symbol': symb,
			 'val': null};
	  socket.send(JSON.stringify(msg));
	}
}

function modelExpand() {
	prepareIDP();
	msg = {'method': 'modelexpand',
		 'number': 3};
	socket.send(JSON.stringify(msg));
}

function propagate() {
  prepareIDP();
  let throwId = Object.keys(playerThrows).length;
  if (throwId === 3) {
    throwId = 0;
  }
  msg = {'method': 'propagate',
         'symbol': 'Throw' + (throwId + 1)}
  socket.send(JSON.stringify(msg));
}

initSocket()